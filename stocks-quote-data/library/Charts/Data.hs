module Charts.Data (plotChart) where

import Data.Foldable (traverse_, toList)
import Graphics.Rendering.Chart.Easy (plot, line, (.=), layout_title)
import Graphics.Rendering.Chart.Backend.Diagrams ( toFile
                                                 , loadSansSerifFonts
                                                 , FileOptions(..)
                                                 , FileFormat (SVG)
                                                 )
import System.FilePath (FilePath)
import GHC.TypeLits (KnownNat, Nat, KnownSymbol, Symbol, symbolVal)

import qualified Quotes.Data as Q

plotChart :: forall f (sym :: Symbol) (n :: Nat) . (Functor f, Foldable f, KnownSymbol sym, KnownNat n) => String
          -> f (Q.Quote sym n)
          -> [Q.Field]
          -> FilePath
          -> IO ()
plotChart title quotes qfs fname = toFile  fileOptions fname do
  layout_title .= title
  traverse_ plotLine qfs
    where
      showSymbol = symbolVal @sym (undefined)
      fileOptions = FileOptions (800, 600) SVG loadSansSerifFonts
      plotLine qf = plot $ line (showSymbol <> " - " <> show qf) [toList . fmap (qf2pd qf) $ quotes]
      qf2pd qf q = (Q.day q, Q.quoteField q qf (\x -> realToFrac x :: Double))
