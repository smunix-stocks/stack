{-# LANGUAGE TemplateHaskell #-}
module Config.Data ( Params(..)
                   , Command(..)
                   , Source(..)
                   , cli
                   ) where

import Data.Semigroup ((<>))
import Options.Applicative ( long, short, Parser, metavar, switch, strArgument, help, helper
                           , progDesc, fullDesc, info, command, subparser, execParser)
import Data.Singletons.TH (singletons)
import Data.Singletons.Prelude
import GHC.TypeLits
import Data.Text (Text)
import Data.ByteString.Char8 (ByteString)
import Data.Time (Day, parseTimeM, defaultTimeLocale)
import Data.Strings (text, bytes)

$(singletons [d|
              data Source = NASDAQ | GOOGLE
              |])
deriving instance Show Source
deriving instance Eq Source

-- cli parameters
data Command (s :: Source) (sym :: Symbol) where
  Command :: { pricesChart  :: Bool
             , volumesChart :: Bool
             , getUrl       :: Day -> Day -> ByteString
             , getHeader    :: ByteString
             } -> Command s sym
  -- deriving (Show, Eq)

data Params where
  Params :: { symbol        :: String
            , pricesChart'  :: Bool
            , volumesChart' :: Bool
            } -> Params
  deriving (Show, Eq)

parseSource :: Parser Source
parseSource = subparser $
  command "nasdaq" (info (helper <*> (pure NASDAQ)) $ progDesc "fetch historical data from NASDAQ, i.e. \"https://www.nasdaq.com/api/v1/historical/AAPL/stocks/2010-01-01/2020-01-01\"") <>
  command "google" (info (helper <*> (pure GOOGLE)) $ progDesc "fetch historical data from GOOGLE")

parseParams :: Parser Params
parseParams = do
  symbol <- strArgument (metavar "SYMBOL")
  pricesChart' <- switch (long "pricesChart" <>
                          short 'p' <>
                          help "create prices chart")
  volumesChart' <- switch (long "volumesChart" <>
                           short 'v' <>
                           help "create volumes chart")
  return Params{..}

cli :: (forall (s :: Source) (sym :: Symbol) . (KnownSymbol sym) => Command s sym -> IO ()) -> IO ()
cli fn = do
  (source, params) <- execParser $
                      info
                      (helper <*> (do
                                      source <- parseSource
                                      params <- parseParams
                                      return (source, params)
                                  ))
                      (fullDesc <>
                       progDesc "stock quotes data processing")
  let
    pricesChart = pricesChart' params
    volumesChart = volumesChart' params

  withSomeSing
    source
    \(Sing :: Sing s) ->
      case someSymbolVal (symbol params) of
        SomeSymbol (undefined :: Proxy sym) -> (fn (Command { pricesChart = pricesChart
                                                            , volumesChart = volumesChart
                                                            , getUrl = \ s e -> "https://www.nasdaq.com/api/v1/historical/" <>
                                                                                 bytes (symbolVal @sym Proxy) <>
                                                                                 "/stocks/" <>
                                                                                 bytes (show s) <>
                                                                                 "/" <>
                                                                                 bytes (show e)
                                                            , getHeader = "Date,Close/Last,Volume,Open,High,Low"
                                                            } :: Command s sym))
