-- | An example module.
module Example (main) where

import Network.HTTP.Simple as Http
import qualified Data.ByteString.Lazy.Char8 as L8
import Data.Time.Calendar
import GHC.TypeLits
import Data.Strings
import Data.Csv (decodeByName)
import Data.Vector (Vector)

import Quotes.Data as Quotes
import Charts.Data as Charts
import Config.Data as Config
import Stats.Data as Stats

run :: forall (src :: Config.Source) (sym :: Symbol) . (KnownSymbol sym) => Command src sym  -> IO ()
run Config.Command{..} = do
  req <- Http.parseRequest (sToString $ getUrl (fromGregorian 2010 01 01) (fromGregorian 2020 01 02))
  res <- Http.httpLBS req
  -- print req
  -- print $ getResponseHeaders res
  let
    csv = body res
  L8.writeFile
    (symbolVal @sym undefined <> ".csv")
    csv
  case decodeByName csv of
    Left err -> print err
    Right qs@(header, (quotes :: Vector (Quotes.Quote sym 4))) -> do
      -- print (qs)
      print (Stats.statistic quotes)
    where
      body res = L8.intercalate (lazyBytes "\n") $ (lazyBytes . sToString $ getHeader) : (drop 1 . sSplitAll (lazyBytes "\n") $ getResponseBody res)

-- | An example function.
main :: IO ()
main = Config.cli run
