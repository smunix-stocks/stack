{-# LANGUAGE TemplateHaskell #-}
module Quotes.Data ( Quote(..)
                   , Decimal(..)
                   , Field(..)
                   , quoteField
                   , withField
                   , BoundedEnum(..)
                   ) where

import Data.Fixed (HasResolution(..), Fixed)
import Data.Time (Day, parseTimeM, defaultTimeLocale)
import Data.Csv (FromNamedRecord(..), FromField(..), (.:))
import qualified Data.ByteString.Char8 as C8
import Data.ByteString.Char8 (unpack)
import Safe (readDef)
import Data.Coerce (coerce)

import GHC.TypeLits ( KnownNat, Nat, natVal
                    , KnownSymbol, Symbol, symbolVal
                    )
import GHC.Generics (Generic)
import Data.Singletons.Prelude
import Data.Singletons.TH
import Data.Strings as S (sTrim, sDrop, sPadLeft, sPadRight)

import Data.Proxy (Proxy(Proxy ))

-- | our decimal precision level
data Precision (n :: Nat)

instance (KnownNat n) => HasResolution (Precision n) where
  resolution _ = floor $ 10 ** (fromIntegral $ natVal @n (undefined))

-- | convert Decimal terms from Csv files
--     12/31/2019, $293.65, 25247630, $289.93, $293.68, $289.52
instance (KnownNat n) => FromField (Decimal n) where
  parseField = pure . readDef 0 . unpack . S.sDrop 1 . sTrim

-- | convert Day terms from Csv files
--     12/31/2019, $293.65, 25247630, $289.93, $293.68, $289.52
instance FromField Day where
  parseField = parseTimeM False defaultTimeLocale "%m/%d/%Y" . unpack

type Decimal (n :: Nat) = Fixed (Precision n)

newtype Volume = Volume Int
  deriving (Show, Eq, Ord, Num)

instance Real Volume where
  toRational (Volume x) = toRational x

instance FromField Volume where
  parseField s = Volume <$> parseField s

-- | a quote definition (follow NASDAQ historical CSV format)
data Quote (s :: Symbol) (n :: Nat) where
  Quote :: { day    :: Day
           , close  :: Decimal n
           , open   :: Decimal n
           , high   :: Decimal n
           , low    :: Decimal n
           , volume :: Volume
           } -> Quote s n
  deriving (Show, Eq, Generic)

-- Date, Close/Last, Volume, Open, High, Low
instance (KnownSymbol s, KnownNat n) => FromNamedRecord (Quote s n) where
  parseNamedRecord m = do
    day    <- m .: ("Date")
    close  <- m .: ("Close/Last")
    volume <- m .: ("Volume")
    open   <- m .: ("Open")
    high   <- m .: ("High")
    low    <- m .: ("Low")
    pure Quote{..}

class (Enum a, Bounded a) => BoundedEnum a where
  range :: [a]
  range = enumFrom minBound

-- | quote enumerations for supported fields
$(singletons [d|
              data Field = CloseF | OpenF | HighF | LowF | VolumeF
               |])
deriving instance Show Field
deriving instance Eq Field
deriving instance Enum Field
deriving instance Bounded Field
deriving instance BoundedEnum Field

withField :: forall (field :: Field) sym n r . (SingI field, KnownSymbol sym, KnownNat n) => (forall ft . Real ft => Quote sym n -> ft -> r) -> Quote sym n -> r
withField f q = go (sing :: Sing field)
  where
    go (fromSing -> CloseF)  = f q (close q)
    go (fromSing -> OpenF)   = f q (open q)
    go (fromSing -> HighF)   = f q (high q)
    go (fromSing -> LowF)    = f q (low q)
    go (fromSing -> VolumeF) = f q (volume q)

quoteField :: forall s n r . (KnownSymbol s, KnownNat n) => Quote s n -> Field -> (forall ft . Real ft => ft -> r) -> r
quoteField q fd fn = withSomeSing fd go
  where
    go :: forall (fd :: Field) . Sing fd -> r
    go (Sing :: Sing fd) = withField @fd (\q f -> fn f) q
