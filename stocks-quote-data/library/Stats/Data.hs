{-# LANGUAGE TemplateHaskell #-}
module Stats.Data ( Statistic(..)
                  , statistic
                  ) where

import GHC.TypeLits (Symbol, Nat, KnownNat, KnownSymbol, symbolVal)
import Data.Singletons.Prelude hiding (Min, Max)
import Data.Singletons.TH (singletons)
import Data.Foldable (maximumBy
                     , minimumBy
                     , toList
                     )
import Data.Time (diffDays)
import Data.Function (on)
import Quotes.Data as Q

$(singletons [d|
               data Statistic = MeanS | MinS | MaxS | DayS
             |])

deriving instance Show Statistic
deriving instance Eq Statistic
deriving instance Enum Statistic
deriving instance Bounded Statistic
deriving instance Q.BoundedEnum Statistic

data Entry where
  Entry :: forall
           (stat :: Statistic)
           (field :: Q.Field)
           (sym :: Symbol)
           (n :: Nat)
           r
           . (SingI stat, Fractional r, Show r) =>
    { value :: Decimal n
    , showEntry :: Decimal n -> String
    } -> Entry

instance Show Entry where
  show (Entry r sf) = sf r

statistic :: forall
             (sym :: Symbol)
             (n :: Nat)
             f
             r
             . (Foldable f, Functor f, KnownNat n, KnownSymbol sym, Fractional r) =>
             f (Q.Quote sym n)
          -> [Entry]
statistic quotes = do
  stat  <- Q.range :: [Statistic]
  field <- Q.range :: [Q.Field]
  return $ withSomeSing field
    \(sf@Sing :: Sing field) ->
      withSomeSing stat
      \(ss@Sing :: Sing stat) ->
        Entry @stat @field @sym @n (compute @stat @field) (\v -> "Stats " <> show (fromSing ss) <> "/" <> show (fromSing sf) <> ":" <> show v)
          where
            compute :: forall (stat :: Statistic) (field :: Q.Field) r . (KnownNat n, SingI field, SingI stat, Fractional r, Ord r) => r
            compute = withStatistic @stat (fmap (Q.withField @field @sym @n (\q f -> (q, realToFrac f))) quotes)

            withStatistic :: forall (stat :: Statistic) t r . (Foldable t, SingI stat, Fractional r, Ord r) => t (Q.Quote sym n, r) -> r
            withStatistic xs = go (sing :: Sing stat)
              where
                go (fromSing -> MeanS) = acc / len
                  where
                    (acc, len) = foldl (\(a, l) (_, f) -> (a+f, l+1)) (0, 0) xs
                go (fromSing -> MinS) = snd $ minimumBy (on compare snd) xs
                go (fromSing -> MaxS) =  snd $ maximumBy (on compare snd) xs
                go (fromSing -> DayS) = realToFrac $ (on diffDays day) dMaxQ dMinQ
                  where
                    (dMinQ, _) = minimumBy (on compare snd) xs
                    (dMaxQ, _) = maximumBy (on compare snd) xs
